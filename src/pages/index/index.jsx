import { Component } from 'react'
import { connect } from 'react-redux'
import { View, Text, Input } from '@tarojs/components'
import './index.scss'
import { add, del } from '../../actions/index'

class Index extends Component {
  config = {
    navigationBarTitleText: "sss"
  }
  state = {
    newTodo: ""
  }
  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }
  saveNewTodo(e) {
    let val = e.detail.value
    console.log(val)
    this.setState({
      newTodo: val
    })
  }
  delTodo(id) {
    console.log(id)
  }
  addTodo() {
    let { add } = this.props;
    let val = this.state.newTodo;
    add(val)
    console.log(val)
  }
  render() {
    let { newTodo } = this.state;
    let { todos, add, del } = this.props;
    console.log(this.props)
    const todosJsx = todos.map(i => {
      return (
        <View className="todos_item"><Text>{i.text}</Text><View className="del" onClick={this.delTodo.bind(this, i.id)}>-</View></View>
      )
    })
    return (
      <View className='index'>
        <View>
          <Input placeholder="填写新的todo" onBlur={this.saveNewTodo.bind(this)} value={newTodo}></Input>
          <View onClick={this.addTodo.bind(this)}>+</View>
        </View>
        <View>{newTodo}</View>
        <View>{todosJsx}</View>
      </View>
    )
  }
}

export default connect(({ todos }) => ({ todos: todos.todos }), dispatch => ({
  add(data) {
    dispatch(add(data))
  },
  del(id) {
    dispatch(del(id))
  }
}))(Index)